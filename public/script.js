$(document).ready(
    function() {
        const currentDate = new Date();
        let currentDateString = currentDate.toLocaleDateString('sv-SE', {year: 'numeric', month: '2-digit', day: '2-digit'});
        let toDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1);
        let toDateString = toDate.toLocaleDateString('sv-SE', {year: 'numeric', month: '2-digit', day: '2-digit'});
    
        let menus = [];
        fetchData();

        function fetchData(){
            let spinner = document.querySelector('#cc-spinner');
            $(spinner).show();  
            fetch('https://api.menyit.se/v1/menus?' + new URLSearchParams({FromDate: currentDateString, ToDate: toDateString}), 
                { headers: { 'X-API-Key': '8k32sAaqTkJU7vD/q4Ob/nRgsul4gI+IFRkawuENLHg=' }
            }).then((res) => res.json())
                .then((value) => {
                    menus = value;
                    showMenus();
                    spinner.style.display = 'none';
                });
        }
        
        const dateControl = document.querySelector('input[type="date"]');    
        dateControl.value = currentDateString;
        
        $(dateControl).on('change', (value) => {
            currentDateString = value.target.value;
            const dateTo = new Date(currentDateString);
            dateTo.setDate(dateTo.getDate() + 1)
            toDateString = dateTo.toLocaleDateString('sv-SE', {year: 'numeric', month: '2-digit', day: '2-digit'});
            fetchData();
        });

        function showMenus(){
            let menuHeaderContainer = document.querySelector('#menu-header-container .nav');
            $(menuHeaderContainer).empty();
            let menuContentContainer = document.querySelector('#menuTabContent');
            $(menuContentContainer).empty();

            if (menus.filter(x => x.title === 'Meny').length === 0) {
                $(menuContentContainer).append(`<h5 style="text-align: center;">Vi har ingen meny för detta datum ännu</h5>`);
            }
            menus.filter(x => x.title === 'Meny').forEach((menu, index) => {        
                let activeClass = index === 0 ? 'active' : '';
                let showClass = index === 0 ? 'show' : '';
                $(menuHeaderContainer).append(`
                <li class="nav-item" role="presentation">
                    <button class="nav-link ${activeClass}" role="tab" id="${menu.title}-tab" data-bs-toggle="tab" data-bs-target="#${menu.title}-tab-pain">${menu.title}</button>
                </li>`);
                let menuItemsContainer = $(`<div id="menu-items-container-${menu.title}" class="menu-items-container row row-cols-1 row-cols-md-3 g-4"><b>Denna meny innehåller inga måltider att beställa</b></div>`);
                $(menuContentContainer).append($(`
                    <div class="tab-pane fade ${showClass} ${activeClass}" id="${menu.title}-tab-pain" role="tabpanel" aria-labelledby="${menu.title}-tab" tabindex="0"></div>
                `).append(menuItemsContainer));
    
                if(menu.items.length > 0) {
                    menuItemsContainer.empty();
                }
    
                menu.items.forEach(meal => {
                    addMealToMenu(menuItemsContainer, meal);
                });
            });
        }
    
        function addMealToMenu(menuItemsContainer, meal){
            let imageUrl = meal.imageUrl != null? meal.imageUrl : 'img/placeholder.jpg';
            menuItemsContainer.append($(`<div id="meal-${meal.id}" class="card">
            <img src="${imageUrl}" width="100%" alt="...">
            <div class="card-body">
              <h5 class="card-title">${meal.title}</h5></div>
          </div>`));
    
          let cardBody = menuItemsContainer.find('#meal-' + meal.id + ' .card-body');
    
          if (meal.description != null && meal.description.length > 0){
            $(cardBody).append(`
                <div>
                    <a class="btn" data-bs-toggle="collapse" href="#collapseDescription-${meal.id}" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Beskrivning
                    </a>
                </div>
                <div class="collapse" id="collapseDescription-${meal.id}" style="">${meal.description}</div>`);
            }
    
          if (meal.ingredients.length > 0){
            let ingredientString = meal.ingredients.join(', ');
            $(cardBody).append(`
                <div>
                    <a class="btn" data-bs-toggle="collapse" href="#collapseIngredients-${meal.id}" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Ingredienser
                    </a>
                </div>
                <div class="collapse" id="collapseIngredients-${meal.id}" style="">${ingredientString}</div>`);
            }

            if (meal.allergens.length > 0){
                let allergenString = meal.allergens.join(', ');
                $(cardBody).append(`
                    <div>
                        <a class="btn" data-bs-toggle="collapse" href="#collapseAllergens-${meal.id}" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Allergener
                        </a>
                    </div>
                    <div class="collapse" id="collapseAllergens-${meal.id}" style="">${allergenString}</div>`);
                }
        }
    });